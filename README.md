# WeatherApp

## Description
A simple Weather Application Built With Angular 2

## Usage

1: Clone repo
```
git clone https://gitlab.com/minhaj_mimo/weatherApp.git
```
2: Install packages
```
npm install
```
3: Start server (includes auto refreshing) and gulp watcher
```
npm start
```