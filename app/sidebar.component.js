System.register(['angular2/core', './profile.service', './weatherApp/service', './weatherApp/weather'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, profile_service_1, service_1, weather_1;
    var SidebarComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (profile_service_1_1) {
                profile_service_1 = profile_service_1_1;
            },
            function (service_1_1) {
                service_1 = service_1_1;
            },
            function (weather_1_1) {
                weather_1 = weather_1_1;
            }],
        execute: function() {
            SidebarComponent = (function () {
                function SidebarComponent(_profileService, _weatherService) {
                    this._profileService = _profileService;
                    this._weatherService = _weatherService;
                }
                SidebarComponent.prototype.onSaveNew = function () {
                    var cities = this._weatherService.getWeatherItems().map(function (element) {
                        return element.city;
                    });
                    this._profileService.saveNewProfile(cities);
                };
                SidebarComponent.prototype.onLoadProfile = function (profile) {
                    var _this = this;
                    this._weatherService.clearWeatherItems();
                    for (var i = 0; i < profile.cities.length; i++) {
                        this._weatherService.searchWeatherData(profile.cities[i])
                            .retry()
                            .subscribe(function (data) {
                            var weatherItem = new weather_1.WeatherItem(data.name, data.main.temp, data.dt);
                            _this._weatherService.addWeatherItem(weatherItem);
                        });
                    }
                };
                SidebarComponent.prototype.onDeleteProfile = function (event, profile) {
                    event.stopPropagation();
                    this._profileService.deleteProfile(profile);
                };
                SidebarComponent.prototype.ngOnInit = function () {
                    this.profiles = this._profileService.getProfiles();
                };
                SidebarComponent = __decorate([
                    core_1.Component({
                        selector: 'sidebar',
                        template: "\n            <div class=\"row\">\n                <div class=\"input-field col s5\"></div>\n\n                <div class=\"col s3\">\n                    <button (click)=\"onSaveNew()\" class=\"btn waves-effect waves-light\" type=\"button\">Save Profile</button>\n                </div>\n            </div>\n\n            <div class=\"row\">\n                <div class=\"col s12\">\n                    <ul class=\"collection with-header\">\n                        <li class=\"collection-header\"><h4>Saved Profile</h4></li>\n                        <li class=\"collection-item\" *ngFor=\"#profile of profiles\" (click)=\"onLoadProfile(profile)\">\n                            <a>\n                            <div class=\"bottomLinks\">\n                                <h5>{{ profile.profileName }}</h5> \n                                {{ profile.cities.join(', ') }} \n                                <a (click)=\"onDeleteProfile($event,profile)\" class=\"secondary-content waves-effect waves-light red btn\">Delete</a>\n                            </div>\n                            </a>\n                        </li>\n                    </ul>\n                </div>\n            </div>\n    ",
                        styleUrls: ['assets/css/sidebar.css'],
                        providers: [profile_service_1.ProfileService]
                    }), 
                    __metadata('design:paramtypes', [profile_service_1.ProfileService, service_1.WeatherService])
                ], SidebarComponent);
                return SidebarComponent;
            }());
            exports_1("SidebarComponent", SidebarComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNpZGViYXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBc0NBO2dCQUdJLDBCQUFxQixlQUErQixFQUFVLGVBQThCO29CQUF2RSxvQkFBZSxHQUFmLGVBQWUsQ0FBZ0I7b0JBQVUsb0JBQWUsR0FBZixlQUFlLENBQWU7Z0JBQUUsQ0FBQztnQkFFL0Ysb0NBQVMsR0FBVDtvQkFDSSxJQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxVQUFTLE9BQW1CO3dCQUNsRixNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztvQkFDeEIsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ2hELENBQUM7Z0JBRUQsd0NBQWEsR0FBYixVQUFjLE9BQWU7b0JBQTdCLGlCQWtCQztvQkFoQkcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO29CQUV6QyxHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFDLENBQUMsRUFBRSxFQUFDLENBQUM7d0JBQ3RDLElBQUksQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQzs2QkFDcEQsS0FBSyxFQUFFOzZCQUNQLFNBQVMsQ0FDTixVQUFBLElBQUk7NEJBQ0EsSUFBTSxXQUFXLEdBQUcsSUFBSSxxQkFBVyxDQUMvQixJQUFJLENBQUMsSUFBSSxFQUNULElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUNkLElBQUksQ0FBQyxFQUFFLENBQ1YsQ0FBQzs0QkFDRixLQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQzt3QkFDckQsQ0FBQyxDQUNKLENBQUM7b0JBQ1YsQ0FBQztnQkFDTCxDQUFDO2dCQUVELDBDQUFlLEdBQWYsVUFBZ0IsS0FBWSxFQUFFLE9BQWdCO29CQUUxQyxLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7b0JBQ3hCLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNoRCxDQUFDO2dCQUVELG1DQUFRLEdBQVI7b0JBQ0ksSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUN2RCxDQUFDO2dCQXhFTDtvQkFBQyxnQkFBUyxDQUFDO3dCQUNQLFFBQVEsRUFBRSxTQUFTO3dCQUNuQixRQUFRLEVBQUUsdXJDQXlCVDt3QkFDRCxTQUFTLEVBQUMsQ0FBQyx3QkFBd0IsQ0FBQzt3QkFDcEMsU0FBUyxFQUFFLENBQUMsZ0NBQWMsQ0FBQztxQkFDOUIsQ0FBQzs7b0NBQUE7Z0JBNkNGLHVCQUFDO1lBQUQsQ0EzQ0EsQUEyQ0MsSUFBQTtZQTNDRCwrQ0EyQ0MsQ0FBQSIsImZpbGUiOiJzaWRlYmFyLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dH0gZnJvbSAnYW5ndWxhcjIvY29yZSc7XHJcbmltcG9ydCB7UHJvZmlsZX0gZnJvbSAnLi9wcm9maWxlJztcclxuaW1wb3J0IHtQcm9maWxlU2VydmljZX0gZnJvbSAnLi9wcm9maWxlLnNlcnZpY2UnO1xyXG5pbXBvcnQge1dlYXRoZXJTZXJ2aWNlfSBmcm9tICcuL3dlYXRoZXJBcHAvc2VydmljZSc7XHJcbmltcG9ydCB7V2VhdGhlckl0ZW19IGZyb20gJy4vd2VhdGhlckFwcC93ZWF0aGVyJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdzaWRlYmFyJyxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1maWVsZCBjb2wgczVcIj48L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sIHMzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiAoY2xpY2spPVwib25TYXZlTmV3KClcIiBjbGFzcz1cImJ0biB3YXZlcy1lZmZlY3Qgd2F2ZXMtbGlnaHRcIiB0eXBlPVwiYnV0dG9uXCI+U2F2ZSBQcm9maWxlPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sIHMxMlwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzcz1cImNvbGxlY3Rpb24gd2l0aC1oZWFkZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwiY29sbGVjdGlvbi1oZWFkZXJcIj48aDQ+U2F2ZWQgUHJvZmlsZTwvaDQ+PC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwiY29sbGVjdGlvbi1pdGVtXCIgKm5nRm9yPVwiI3Byb2ZpbGUgb2YgcHJvZmlsZXNcIiAoY2xpY2spPVwib25Mb2FkUHJvZmlsZShwcm9maWxlKVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGE+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYm90dG9tTGlua3NcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDU+e3sgcHJvZmlsZS5wcm9maWxlTmFtZSB9fTwvaDU+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt7IHByb2ZpbGUuY2l0aWVzLmpvaW4oJywgJykgfX0gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgKGNsaWNrKT1cIm9uRGVsZXRlUHJvZmlsZSgkZXZlbnQscHJvZmlsZSlcIiBjbGFzcz1cInNlY29uZGFyeS1jb250ZW50IHdhdmVzLWVmZmVjdCB3YXZlcy1saWdodCByZWQgYnRuXCI+RGVsZXRlPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgIGAsXHJcbiAgICBzdHlsZVVybHM6Wydhc3NldHMvY3NzL3NpZGViYXIuY3NzJ10sXHJcbiAgICBwcm92aWRlcnM6IFtQcm9maWxlU2VydmljZV1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBTaWRlYmFyQ29tcG9uZW50e1xyXG4gICAgcHJvZmlsZXM6IFByb2ZpbGVbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvciAocHJpdmF0ZSBfcHJvZmlsZVNlcnZpY2U6IFByb2ZpbGVTZXJ2aWNlLCBwcml2YXRlIF93ZWF0aGVyU2VydmljZTpXZWF0aGVyU2VydmljZSl7fVxyXG5cclxuICAgIG9uU2F2ZU5ldygpe1xyXG4gICAgICAgIGNvbnN0IGNpdGllcyA9IHRoaXMuX3dlYXRoZXJTZXJ2aWNlLmdldFdlYXRoZXJJdGVtcygpLm1hcChmdW5jdGlvbihlbGVtZW50OldlYXRoZXJJdGVtKXtcclxuICAgICAgICAgICAgcmV0dXJuIGVsZW1lbnQuY2l0eTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLl9wcm9maWxlU2VydmljZS5zYXZlTmV3UHJvZmlsZShjaXRpZXMpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uTG9hZFByb2ZpbGUocHJvZmlsZTpQcm9maWxlKVxyXG4gICAge1xyXG4gICAgICAgIHRoaXMuX3dlYXRoZXJTZXJ2aWNlLmNsZWFyV2VhdGhlckl0ZW1zKCk7XHJcblxyXG4gICAgICAgIGZvcihsZXQgaT0wOyBpPHByb2ZpbGUuY2l0aWVzLmxlbmd0aDtpKyspe1xyXG4gICAgICAgICAgICB0aGlzLl93ZWF0aGVyU2VydmljZS5zZWFyY2hXZWF0aGVyRGF0YShwcm9maWxlLmNpdGllc1tpXSlcclxuICAgICAgICAgICAgICAgIC5yZXRyeSgpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGEgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB3ZWF0aGVySXRlbSA9IG5ldyBXZWF0aGVySXRlbShcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEubmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEubWFpbi50ZW1wLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS5kdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl93ZWF0aGVyU2VydmljZS5hZGRXZWF0aGVySXRlbSh3ZWF0aGVySXRlbSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25EZWxldGVQcm9maWxlKGV2ZW50OiBFdmVudCwgcHJvZmlsZTogUHJvZmlsZSlcclxuICAgIHtcclxuICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICB0aGlzLl9wcm9maWxlU2VydmljZS5kZWxldGVQcm9maWxlKHByb2ZpbGUpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCl7XHJcbiAgICAgICAgdGhpcy5wcm9maWxlcyA9IHRoaXMuX3Byb2ZpbGVTZXJ2aWNlLmdldFByb2ZpbGVzKCk7XHJcbiAgICB9XHJcblxyXG5cclxufSJdfQ==
