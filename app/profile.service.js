System.register(['./profile'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var profile_1;
    var ProfileService;
    return {
        setters:[
            function (profile_1_1) {
                profile_1 = profile_1_1;
            }],
        execute: function() {
            ProfileService = (function () {
                function ProfileService() {
                    this.profiles = [
                        new profile_1.Profile('Default Profile', ['Cox\'sbazar', 'Dhaka'])
                    ];
                }
                ProfileService.prototype.saveNewProfile = function (cities) {
                    var profileName = 'Profile' + this.profiles.length;
                    var profile = new profile_1.Profile(profileName, cities);
                    this.profiles.push(profile);
                };
                ProfileService.prototype.getProfiles = function () {
                    return this.profiles;
                };
                ProfileService.prototype.deleteProfile = function (profile) {
                    this.profiles.splice(this.profiles.indexOf(profile), 1);
                };
                return ProfileService;
            }());
            exports_1("ProfileService", ProfileService);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2ZpbGUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztZQUVBO2dCQUFBO29CQUNZLGFBQVEsR0FBYzt3QkFDMUIsSUFBSSxpQkFBTyxDQUFDLGlCQUFpQixFQUFDLENBQUMsYUFBYSxFQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUN6RCxDQUFDO2dCQWVOLENBQUM7Z0JBYkcsdUNBQWMsR0FBZCxVQUFlLE1BQWU7b0JBQzFCLElBQU0sV0FBVyxHQUFHLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztvQkFDckQsSUFBTSxPQUFPLEdBQUcsSUFBSSxpQkFBTyxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQztvQkFDakQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ2hDLENBQUM7Z0JBRUQsb0NBQVcsR0FBWDtvQkFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDekIsQ0FBQztnQkFFRCxzQ0FBYSxHQUFiLFVBQWMsT0FBZ0I7b0JBQzFCLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMzRCxDQUFDO2dCQUNMLHFCQUFDO1lBQUQsQ0FsQkEsQUFrQkMsSUFBQTtZQWxCRCwyQ0FrQkMsQ0FBQSIsImZpbGUiOiJwcm9maWxlLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1Byb2ZpbGV9IGZyb20gJy4vcHJvZmlsZSc7XHJcblxyXG5leHBvcnQgY2xhc3MgUHJvZmlsZVNlcnZpY2Uge1xyXG4gICAgcHJpdmF0ZSBwcm9maWxlczogUHJvZmlsZVtdID0gW1xyXG4gICAgICAgIG5ldyBQcm9maWxlKCdEZWZhdWx0IFByb2ZpbGUnLFsnQ294XFwnc2JhemFyJywnRGhha2EnXSlcclxuICAgIF07XHJcblxyXG4gICAgc2F2ZU5ld1Byb2ZpbGUoY2l0aWVzOnN0cmluZ1tdKXtcclxuICAgICAgICBjb25zdCBwcm9maWxlTmFtZSA9ICdQcm9maWxlJyArIHRoaXMucHJvZmlsZXMubGVuZ3RoO1xyXG4gICAgICAgIGNvbnN0IHByb2ZpbGUgPSBuZXcgUHJvZmlsZShwcm9maWxlTmFtZSwgY2l0aWVzKTtcclxuICAgICAgICB0aGlzLnByb2ZpbGVzLnB1c2gocHJvZmlsZSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJvZmlsZXMoKXtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9maWxlcztcclxuICAgIH1cclxuXHJcbiAgICBkZWxldGVQcm9maWxlKHByb2ZpbGU6IFByb2ZpbGUpe1xyXG4gICAgICAgIHRoaXMucHJvZmlsZXMuc3BsaWNlKHRoaXMucHJvZmlsZXMuaW5kZXhPZihwcm9maWxlKSwxKTtcclxuICAgIH1cclxufSJdfQ==
