System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Profile;
    return {
        setters:[],
        execute: function() {
            Profile = (function () {
                function Profile(profileName, cities) {
                    this.profileName = profileName;
                    this.cities = cities;
                }
                ;
                return Profile;
            }());
            exports_1("Profile", Profile);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2ZpbGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztZQUFBO2dCQUNJLGlCQUFvQixXQUFtQixFQUFTLE1BQWdCO29CQUE1QyxnQkFBVyxHQUFYLFdBQVcsQ0FBUTtvQkFBUyxXQUFNLEdBQU4sTUFBTSxDQUFVO2dCQUFFLENBQUM7O2dCQUN2RSxjQUFDO1lBQUQsQ0FGQSxBQUVDLElBQUE7WUFGRCw2QkFFQyxDQUFBIiwiZmlsZSI6InByb2ZpbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgUHJvZmlsZXtcclxuICAgIGNvbnN0cnVjdG9yIChwdWJsaWMgcHJvZmlsZU5hbWU6IHN0cmluZywgcHVibGljIGNpdGllczogc3RyaW5nW10pe307XHJcbn0iXX0=
