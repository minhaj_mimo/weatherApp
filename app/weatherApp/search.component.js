System.register(['angular2/core', './service', './weather', 'rxjs/Subject'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, service_1, weather_1, Subject_1;
    var WeatherSearchComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (service_1_1) {
                service_1 = service_1_1;
            },
            function (weather_1_1) {
                weather_1 = weather_1_1;
            },
            function (Subject_1_1) {
                Subject_1 = Subject_1_1;
            }],
        execute: function() {
            WeatherSearchComponent = (function () {
                function WeatherSearchComponent(_weatherService) {
                    this._weatherService = _weatherService;
                    this.searchStream = new Subject_1.Subject();
                    this.data = {};
                }
                WeatherSearchComponent.prototype.onSubmit = function () {
                    var weatherItem = new weather_1.WeatherItem(this.data.name, this.data.main.temp, this.data.dt);
                    this._weatherService.addWeatherItem(weatherItem);
                };
                WeatherSearchComponent.prototype.onSearchLocation = function (city) {
                    this.searchStream
                        .next(city);
                };
                WeatherSearchComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.searchStream
                        .debounceTime(300)
                        .distinctUntilChanged()
                        .switchMap(function (input) { return _this._weatherService.searchWeatherData(input); })
                        .subscribe(function (data) { return _this.data = data; });
                };
                WeatherSearchComponent = __decorate([
                    core_1.Component({
                        selector: 'weather-search',
                        template: "\n        <div class=\"row\">\n            <form class=\"col s12\" (ngSubmit)=\"onSubmit()\">\n                <div class=\"row\">\n                    <div class=\"input-field col s10\">\n                        <input ngControl=\"location\" (input)=\"onSearchLocation(input.value)\" placeholder=\"Enter the name of the city\" id=\"city\" type=\"text\" class=\"validate\" required #input>\n                    </div>\n\n                    <div class=\"input-field col s2\">\n                        <button class=\"btn waves-effect waves-light\" type=\"submit\" name=\"action\">Submit</button>\n                    </div>\n                </div>\n            </form>\n\n            <div class=\"row\">\n                <div class=\"input-field col s4\">\n                </div>\n\n               <div class=\"input-field col s4\">\n                    <div class=\"collection\">\n                        <a class=\"collection-item\">City Match : <span class=\"badge\" >{{ data.name }}</span></a>\n                    </div>\n                </div>\n            </div>\n\n           \n        </div>\n    ",
                    }), 
                    __metadata('design:paramtypes', [service_1.WeatherService])
                ], WeatherSearchComponent);
                return WeatherSearchComponent;
            }());
            exports_1("WeatherSearchComponent", WeatherSearchComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYXRoZXJBcHAvc2VhcmNoLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQXNDQTtnQkFLSSxnQ0FBcUIsZUFBK0I7b0JBQS9CLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtvQkFINUMsaUJBQVksR0FBRyxJQUFJLGlCQUFPLEVBQVUsQ0FBQztvQkFDN0MsU0FBSSxHQUFRLEVBQUUsQ0FBQztnQkFFdUMsQ0FBQztnQkFFdkQseUNBQVEsR0FBUjtvQkFDZ0IsSUFBTSxXQUFXLEdBQUcsSUFBSSxxQkFBVyxDQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFDZCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQ25CLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUNYLENBQUM7b0JBQ04sSUFBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ2pFLENBQUM7Z0JBRUQsaURBQWdCLEdBQWhCLFVBQWlCLElBQVk7b0JBQ3pCLElBQUksQ0FBQyxZQUFZO3lCQUNaLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsQ0FBQztnQkFFRCx5Q0FBUSxHQUFSO29CQUFBLGlCQVFDO29CQVBHLElBQUksQ0FBQyxZQUFZO3lCQUNaLFlBQVksQ0FBQyxHQUFHLENBQUM7eUJBQ2pCLG9CQUFvQixFQUFFO3lCQUN0QixTQUFTLENBQUMsVUFBQyxLQUFZLElBQUssT0FBQSxLQUFJLENBQUMsZUFBZSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxFQUE3QyxDQUE2QyxDQUFDO3lCQUMxRSxTQUFTLENBQ04sVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsSUFBSSxHQUFHLElBQUksRUFBaEIsQ0FBZ0IsQ0FDM0IsQ0FBQztnQkFDVixDQUFDO2dCQTdETDtvQkFBQyxnQkFBUyxDQUFDO3dCQUNQLFFBQVEsRUFBRSxnQkFBZ0I7d0JBQzFCLFFBQVEsRUFBRSxvbENBMkJUO3FCQUNKLENBQUM7OzBDQUFBO2dCQWdDRiw2QkFBQztZQUFELENBOUJBLEFBOEJDLElBQUE7WUE5QkQsMkRBOEJDLENBQUEiLCJmaWxlIjoid2VhdGhlckFwcC9zZWFyY2guY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsT25Jbml0fSBmcm9tICdhbmd1bGFyMi9jb3JlJztcclxuaW1wb3J0IHtDb250cm9sR3JvdXB9IGZyb20gJ2FuZ3VsYXIyL2NvbW1vbic7XHJcbmltcG9ydCB7V2VhdGhlclNlcnZpY2V9IGZyb20gJy4vc2VydmljZSc7XHJcbmltcG9ydCB7V2VhdGhlckl0ZW19IGZyb20gJy4vd2VhdGhlcic7XHJcbmltcG9ydCB7U3ViamVjdH0gZnJvbSAncnhqcy9TdWJqZWN0JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICd3ZWF0aGVyLXNlYXJjaCcsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgPGZvcm0gY2xhc3M9XCJjb2wgczEyXCIgKG5nU3VibWl0KT1cIm9uU3VibWl0KClcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZmllbGQgY29sIHMxMFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgbmdDb250cm9sPVwibG9jYXRpb25cIiAoaW5wdXQpPVwib25TZWFyY2hMb2NhdGlvbihpbnB1dC52YWx1ZSlcIiBwbGFjZWhvbGRlcj1cIkVudGVyIHRoZSBuYW1lIG9mIHRoZSBjaXR5XCIgaWQ9XCJjaXR5XCIgdHlwZT1cInRleHRcIiBjbGFzcz1cInZhbGlkYXRlXCIgcmVxdWlyZWQgI2lucHV0PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZmllbGQgY29sIHMyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJidG4gd2F2ZXMtZWZmZWN0IHdhdmVzLWxpZ2h0XCIgdHlwZT1cInN1Ym1pdFwiIG5hbWU9XCJhY3Rpb25cIj5TdWJtaXQ8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Zvcm0+XHJcblxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZmllbGQgY29sIHM0XCI+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1maWVsZCBjb2wgczRcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sbGVjdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cImNvbGxlY3Rpb24taXRlbVwiPkNpdHkgTWF0Y2ggOiA8c3BhbiBjbGFzcz1cImJhZGdlXCIgPnt7IGRhdGEubmFtZSB9fTwvc3Bhbj48L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgIFxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgYCxcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBXZWF0aGVyU2VhcmNoQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0XHJcbntcclxuICAgIHByaXZhdGUgc2VhcmNoU3RyZWFtID0gbmV3IFN1YmplY3Q8c3RyaW5nPigpO1xyXG4gICAgZGF0YTogYW55ID0ge307XHJcblxyXG4gICAgY29uc3RydWN0b3IgKHByaXZhdGUgX3dlYXRoZXJTZXJ2aWNlOiBXZWF0aGVyU2VydmljZSl7fVxyXG5cclxuICAgIG9uU3VibWl0KCl7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgd2VhdGhlckl0ZW0gPSBuZXcgV2VhdGhlckl0ZW0oXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0YS5uYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGEubWFpbi50ZW1wLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGEuZHRcclxuICAgICAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl93ZWF0aGVyU2VydmljZS5hZGRXZWF0aGVySXRlbSh3ZWF0aGVySXRlbSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25TZWFyY2hMb2NhdGlvbihjaXR5OiBzdHJpbmcpe1xyXG4gICAgICAgIHRoaXMuc2VhcmNoU3RyZWFtXHJcbiAgICAgICAgICAgIC5uZXh0KGNpdHkpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMuc2VhcmNoU3RyZWFtXHJcbiAgICAgICAgICAgIC5kZWJvdW5jZVRpbWUoMzAwKVxyXG4gICAgICAgICAgICAuZGlzdGluY3RVbnRpbENoYW5nZWQoKVxyXG4gICAgICAgICAgICAuc3dpdGNoTWFwKChpbnB1dDpzdHJpbmcpID0+IHRoaXMuX3dlYXRoZXJTZXJ2aWNlLnNlYXJjaFdlYXRoZXJEYXRhKGlucHV0KSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgIGRhdGEgPT4gdGhpcy5kYXRhID0gZGF0YVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
