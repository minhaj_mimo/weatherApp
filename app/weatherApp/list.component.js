System.register(['angular2/core', './item.component', './service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, item_component_1, service_1;
    var ListComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (item_component_1_1) {
                item_component_1 = item_component_1_1;
            },
            function (service_1_1) {
                service_1 = service_1_1;
            }],
        execute: function() {
            ListComponent = (function () {
                function ListComponent(_weatherService) {
                    this._weatherService = _weatherService;
                }
                ListComponent.prototype.ngOnInit = function () {
                    this.weatherItems = this._weatherService.getWeatherItems();
                };
                ;
                ListComponent = __decorate([
                    core_1.Component({
                        selector: 'weather-list',
                        template: "\n        <div class=\"row\">\n            <weather-item *ngFor=\"#weatherItem of weatherItems\" [item]=\"weatherItem\"></weather-item>\n        </div>\n    ",
                        directives: [item_component_1.ItemComponent],
                        providers: [service_1.WeatherService]
                    }), 
                    __metadata('design:paramtypes', [service_1.WeatherService])
                ], ListComponent);
                return ListComponent;
            }());
            exports_1("ListComponent", ListComponent);
        }
    }
});
/****************************************
 * Weather Item List is genereting here *
 ****************************************/ 

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYXRoZXJBcHAvbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFpQkE7Z0JBR0ksdUJBQXFCLGVBQStCO29CQUEvQixvQkFBZSxHQUFmLGVBQWUsQ0FBZ0I7Z0JBQUUsQ0FBQztnQkFFdkQsZ0NBQVEsR0FBUjtvQkFDSSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxFQUFFLENBQUM7Z0JBQy9ELENBQUM7O2dCQWxCTDtvQkFBQyxnQkFBUyxDQUFDO3dCQUNQLFFBQVEsRUFBRSxjQUFjO3dCQUN4QixRQUFRLEVBQUUsK0pBSVQ7d0JBQ0QsVUFBVSxFQUFFLENBQUMsOEJBQWEsQ0FBQzt3QkFDM0IsU0FBUyxFQUFFLENBQUMsd0JBQWMsQ0FBQztxQkFDOUIsQ0FBQzs7aUNBQUE7Z0JBVUYsb0JBQUM7WUFBRCxDQVJBLEFBUUMsSUFBQTtZQVJELHlDQVFDLENBQUE7Ozs7QUFHRDs7MENBRTBDIiwiZmlsZSI6IndlYXRoZXJBcHAvbGlzdC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCxPbkluaXR9IGZyb20gJ2FuZ3VsYXIyL2NvcmUnO1xyXG5pbXBvcnQge0l0ZW1Db21wb25lbnR9IGZyb20gJy4vaXRlbS5jb21wb25lbnQnO1xyXG5pbXBvcnQge1dlYXRoZXJJdGVtfSBmcm9tICcuL3dlYXRoZXInO1xyXG5pbXBvcnQge1dFQVRIRVJfSVRFTVN9IGZyb20gJy4vZGF0YSc7XHJcbmltcG9ydCB7V2VhdGhlclNlcnZpY2V9IGZyb20gJy4vc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnd2VhdGhlci1saXN0JyxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICAgICAgICA8d2VhdGhlci1pdGVtICpuZ0Zvcj1cIiN3ZWF0aGVySXRlbSBvZiB3ZWF0aGVySXRlbXNcIiBbaXRlbV09XCJ3ZWF0aGVySXRlbVwiPjwvd2VhdGhlci1pdGVtPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgYCxcclxuICAgIGRpcmVjdGl2ZXM6IFtJdGVtQ29tcG9uZW50XSxcclxuICAgIHByb3ZpZGVyczogW1dlYXRoZXJTZXJ2aWNlXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIExpc3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXR7XHJcbiAgICB3ZWF0aGVySXRlbXMgOiBXZWF0aGVySXRlbVtdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yIChwcml2YXRlIF93ZWF0aGVyU2VydmljZTogV2VhdGhlclNlcnZpY2Upe31cclxuXHJcbiAgICBuZ09uSW5pdCgpOmFueXtcclxuICAgICAgICB0aGlzLndlYXRoZXJJdGVtcyA9IHRoaXMuX3dlYXRoZXJTZXJ2aWNlLmdldFdlYXRoZXJJdGVtcygpO1xyXG4gICAgfTtcclxufVxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqIFdlYXRoZXIgSXRlbSBMaXN0IGlzIGdlbmVyZXRpbmcgaGVyZSAqXHJcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqLyJdfQ==
