System.register(['angular2/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var ItemComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            ItemComponent = (function () {
                function ItemComponent() {
                }
                ItemComponent = __decorate([
                    core_1.Component({
                        selector: 'weather-item',
                        template: "\n        <div class=\"col s12 m6 l4\">\n          <div class=\"card blue darken-3\">\n            <div class=\"card-content white-text\">\n              <span class=\"card-title\">{{ weatherItem.city }}</span>\n              <h3>{{ weatherItem.temperature }}</h3>\n              <p> {{ weatherItem.date | date:'yyyy-MM-dd HH:mm:ss Z' }} </p>\n            </div>\n          </div>\n        </div>\n    ",
                        inputs: ['weatherItem: item']
                    }), 
                    __metadata('design:paramtypes', [])
                ], ItemComponent);
                return ItemComponent;
            }());
            exports_1("ItemComponent", ItemComponent);
        }
    }
});
/***************************************************************
 * Component for weather items. The cards are genereting here. *
 ***************************************************************/ 

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYXRoZXJBcHAvaXRlbS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFtQkE7Z0JBQUE7Z0JBUUEsQ0FBQztnQkF4QkQ7b0JBQUMsZ0JBQVMsQ0FBQzt3QkFDUCxRQUFRLEVBQUUsY0FBYzt3QkFDeEIsUUFBUSxFQUFFLG9aQVVUO3dCQUNELE1BQU0sRUFBQyxDQUFDLG1CQUFtQixDQUFDO3FCQUMvQixDQUFDOztpQ0FBQTtnQkFVRixvQkFBQztZQUFELENBUkEsQUFRQyxJQUFBO1lBUkQseUNBUUMsQ0FBQTs7OztBQUVEOztpRUFFaUUiLCJmaWxlIjoid2VhdGhlckFwcC9pdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dH0gZnJvbSAnYW5ndWxhcjIvY29yZSc7XHJcbmltcG9ydCB7V2VhdGhlckl0ZW19IGZyb20gJy4vd2VhdGhlcic7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnd2VhdGhlci1pdGVtJyxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNvbCBzMTIgbTYgbDRcIj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjYXJkIGJsdWUgZGFya2VuLTNcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNhcmQtY29udGVudCB3aGl0ZS10ZXh0XCI+XHJcbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJjYXJkLXRpdGxlXCI+e3sgd2VhdGhlckl0ZW0uY2l0eSB9fTwvc3Bhbj5cclxuICAgICAgICAgICAgICA8aDM+e3sgd2VhdGhlckl0ZW0udGVtcGVyYXR1cmUgfX08L2gzPlxyXG4gICAgICAgICAgICAgIDxwPiB7eyB3ZWF0aGVySXRlbS5kYXRlIHwgZGF0ZToneXl5eS1NTS1kZCBISDptbTpzcyBaJyB9fSA8L3A+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICBgLFxyXG4gICAgaW5wdXRzOlsnd2VhdGhlckl0ZW06IGl0ZW0nXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEl0ZW1Db21wb25lbnR7XHJcbiAgICAvLyBASW5wdXQoJ2l0ZW0nKSB3ZWF0aGVySXRlbTogV2VhdGhlckl0ZW07XHJcblxyXG4gICAgLy93ZWF0aGVySXRlbSA6IFdlYXRoZXJJdGVtO1xyXG5cclxuICAgIC8vIGNvbnN0cnVjdG9yKCl7XHJcbiAgICAvLyAgICAgdGhpcy53ZWF0aGVySXRlbSA9IG5ldyBXZWF0aGVySXRlbSgnQmFuZ2xhZGVzaCcsMzIsJzEwIGFtIFN1bmRheSwgMTEvMTUvMjAxNicpO1xyXG4gICAgLy8gfVxyXG59XHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqIENvbXBvbmVudCBmb3Igd2VhdGhlciBpdGVtcy4gVGhlIGNhcmRzIGFyZSBnZW5lcmV0aW5nIGhlcmUuICpcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi8iXX0=
