System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var WeatherItem;
    return {
        setters:[],
        execute: function() {
            WeatherItem = (function () {
                function WeatherItem(city, temperature, date) {
                    this.city = city;
                    this.temperature = temperature;
                    this.date = date;
                }
                return WeatherItem;
            }());
            exports_1("WeatherItem", WeatherItem);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYXRoZXJBcHAvd2VhdGhlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O1lBQUE7Z0JBQ0kscUJBQW9CLElBQVksRUFBUyxXQUFtQixFQUFTLElBQVc7b0JBQTVELFNBQUksR0FBSixJQUFJLENBQVE7b0JBQVMsZ0JBQVcsR0FBWCxXQUFXLENBQVE7b0JBQVMsU0FBSSxHQUFKLElBQUksQ0FBTztnQkFFaEYsQ0FBQztnQkFDTCxrQkFBQztZQUFELENBSkEsQUFJQyxJQUFBO1lBSkQscUNBSUMsQ0FBQSIsImZpbGUiOiJ3ZWF0aGVyQXBwL3dlYXRoZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgV2VhdGhlckl0ZW17XHJcbiAgICBjb25zdHJ1Y3RvciggcHVibGljIGNpdHk6IHN0cmluZywgcHVibGljIHRlbXBlcmF0dXJlOiBudW1iZXIsIHB1YmxpYyBkYXRlOnN0cmluZyApe1xyXG5cclxuICAgIH1cclxufSJdfQ==
