System.register(['angular2/core', './data', 'rxjs/Observable', 'angular2/http', 'rxjs/Rx'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, data_1, Observable_1, http_1;
    var WeatherService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (data_1_1) {
                data_1 = data_1_1;
            },
            function (Observable_1_1) {
                Observable_1 = Observable_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {}],
        execute: function() {
            WeatherService = (function () {
                function WeatherService(_http) {
                    this._http = _http;
                }
                WeatherService.prototype.getWeatherItems = function () {
                    return data_1.WEATHER_ITEMS;
                };
                WeatherService.prototype.addWeatherItem = function (weatherItem) {
                    data_1.WEATHER_ITEMS.push(weatherItem);
                };
                WeatherService.prototype.clearWeatherItems = function () {
                    data_1.WEATHER_ITEMS.splice(0);
                };
                WeatherService.prototype.searchWeatherData = function (city) {
                    return this._http.get('http://api.openweathermap.org/data/2.5/weather?q=' + city + '&APPID=ec30f129863cc999b64e9b2e271355d3&units=metric')
                        .map(function (response) { return response.json(); })
                        .catch(function (error) {
                        console.error(error);
                        return Observable_1.Observable.throw(error.json());
                    });
                };
                WeatherService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], WeatherService);
                return WeatherService;
            }());
            exports_1("WeatherService", WeatherService);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYXRoZXJBcHAvc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFTQTtnQkFFSSx3QkFBb0IsS0FBVztvQkFBWCxVQUFLLEdBQUwsS0FBSyxDQUFNO2dCQUFFLENBQUM7Z0JBRWxDLHdDQUFlLEdBQWY7b0JBQ0ksTUFBTSxDQUFDLG9CQUFhLENBQUM7Z0JBQ3pCLENBQUM7Z0JBRUQsdUNBQWMsR0FBZCxVQUFlLFdBQXdCO29CQUVuQyxvQkFBYSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDcEMsQ0FBQztnQkFFRCwwQ0FBaUIsR0FBakI7b0JBQ0ksb0JBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzVCLENBQUM7Z0JBRUQsMENBQWlCLEdBQWpCLFVBQWtCLElBQVk7b0JBQzFCLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxtREFBbUQsR0FBRSxJQUFJLEdBQUUsc0RBQXNELENBQUM7eUJBQ3ZJLEdBQUcsQ0FBQyxVQUFBLFFBQVEsSUFBSSxPQUFBLFFBQVEsQ0FBQyxJQUFJLEVBQUUsRUFBZixDQUFlLENBQUM7eUJBQ2hDLEtBQUssQ0FBQyxVQUFBLEtBQUs7d0JBQ1IsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDckIsTUFBTSxDQUFDLHVCQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFBO29CQUN6QyxDQUFDLENBQUMsQ0FBQztnQkFDUCxDQUFDO2dCQTFCTDtvQkFBQyxpQkFBVSxFQUFFOztrQ0FBQTtnQkEyQmIscUJBQUM7WUFBRCxDQXpCQSxBQXlCQyxJQUFBO1lBekJELDJDQXlCQyxDQUFBIiwiZmlsZSI6IndlYXRoZXJBcHAvc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnYW5ndWxhcjIvY29yZSc7XHJcbmltcG9ydCB7V0VBVEhFUl9JVEVNU30gZnJvbSAnLi9kYXRhJztcclxuaW1wb3J0IHtPYnNlcnZhYmxlfSBmcm9tICdyeGpzL09ic2VydmFibGUnO1xyXG5pbXBvcnQge0h0dHB9IGZyb20gJ2FuZ3VsYXIyL2h0dHAnO1xyXG5pbXBvcnQge1dlYXRoZXJJdGVtfSBmcm9tICcuL3dlYXRoZXInO1xyXG5pbXBvcnQgJ3J4anMvUngnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5cclxuZXhwb3J0IGNsYXNzIFdlYXRoZXJTZXJ2aWNlXHJcbntcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2h0dHA6IEh0dHApe31cclxuXHJcbiAgICBnZXRXZWF0aGVySXRlbXMoKXtcclxuICAgICAgICByZXR1cm4gV0VBVEhFUl9JVEVNUztcclxuICAgIH1cclxuXHJcbiAgICBhZGRXZWF0aGVySXRlbSh3ZWF0aGVySXRlbTogV2VhdGhlckl0ZW0pXHJcbiAgICB7XHJcbiAgICAgICAgV0VBVEhFUl9JVEVNUy5wdXNoKHdlYXRoZXJJdGVtKTtcclxuICAgIH1cclxuXHJcbiAgICBjbGVhcldlYXRoZXJJdGVtcygpe1xyXG4gICAgICAgIFdFQVRIRVJfSVRFTVMuc3BsaWNlKDApO1xyXG4gICAgfVxyXG5cclxuICAgIHNlYXJjaFdlYXRoZXJEYXRhKGNpdHk6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PntcclxuICAgICAgICByZXR1cm4gdGhpcy5faHR0cC5nZXQoJ2h0dHA6Ly9hcGkub3BlbndlYXRoZXJtYXAub3JnL2RhdGEvMi41L3dlYXRoZXI/cT0nKyBjaXR5ICsnJkFQUElEPWVjMzBmMTI5ODYzY2M5OTliNjRlOWIyZTI3MTM1NWQzJnVuaXRzPW1ldHJpYycpXHJcbiAgICAgICAgLm1hcChyZXNwb25zZSA9PiByZXNwb25zZS5qc29uKCkpXHJcbiAgICAgICAgLmNhdGNoKGVycm9yID0+e1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgcmV0dXJuIE9ic2VydmFibGUudGhyb3coZXJyb3IuanNvbigpKVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59Il19
