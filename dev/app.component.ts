import {Component} from 'angular2/core';
import {ListComponent} from './weatherApp/list.component';
import {WeatherSearchComponent} from './weatherApp/search.component';
import {SidebarComponent} from './sidebar.component';

@Component({
    selector: 'my-app',
    template: `
        <weather-search></weather-search>
        <weather-list></weather-list>
        <sidebar></sidebar>
    `,
    directives: [ListComponent,WeatherSearchComponent,SidebarComponent]
})
export class AppComponent {

}