import {Component,OnInit} from 'angular2/core';
import {ControlGroup} from 'angular2/common';
import {WeatherService} from './service';
import {WeatherItem} from './weather';
import {Subject} from 'rxjs/Subject';

@Component({
    selector: 'weather-search',
    template: `
        <div class="row">
            <form class="col s12" (ngSubmit)="onSubmit()">
                <div class="row">
                    <div class="input-field col s10">
                        <input ngControl="location" (input)="onSearchLocation(input.value)" placeholder="Enter the name of the city" id="city" type="text" class="validate" required #input>
                    </div>

                    <div class="input-field col s2">
                        <button class="btn waves-effect waves-light" type="submit" name="action">Submit</button>
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="input-field col s4">
                </div>

               <div class="input-field col s4">
                    <div class="collection">
                        <a class="collection-item">City Match : <span class="badge" >{{ data.name }}</span></a>
                    </div>
                </div>
            </div>

           
        </div>
    `,
})

export class WeatherSearchComponent implements OnInit
{
    private searchStream = new Subject<string>();
    data: any = {};

    constructor (private _weatherService: WeatherService){}

    onSubmit(){
                    const weatherItem = new WeatherItem(
                        this.data.name,
                        this.data.main.temp,
                        this.data.dt
                        );
                    this._weatherService.addWeatherItem(weatherItem);
    }

    onSearchLocation(city: string){
        this.searchStream
            .next(city);
    }

    ngOnInit() {
        this.searchStream
            .debounceTime(300)
            .distinctUntilChanged()
            .switchMap((input:string) => this._weatherService.searchWeatherData(input))
            .subscribe(
                data => this.data = data
            );
    }
}
