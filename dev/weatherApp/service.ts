import {Injectable} from 'angular2/core';
import {WEATHER_ITEMS} from './data';
import {Observable} from 'rxjs/Observable';
import {Http} from 'angular2/http';
import {WeatherItem} from './weather';
import 'rxjs/Rx';

@Injectable()

export class WeatherService
{
    constructor(private _http: Http){}

    getWeatherItems(){
        return WEATHER_ITEMS;
    }

    addWeatherItem(weatherItem: WeatherItem)
    {
        WEATHER_ITEMS.push(weatherItem);
    }

    clearWeatherItems(){
        WEATHER_ITEMS.splice(0);
    }

    searchWeatherData(city: string): Observable<any>{
        return this._http.get('http://api.openweathermap.org/data/2.5/weather?q='+ city +'&APPID=ec30f129863cc999b64e9b2e271355d3&units=metric')
        .map(response => response.json())
        .catch(error =>{
            console.error(error);
            return Observable.throw(error.json())
        });
    }
}