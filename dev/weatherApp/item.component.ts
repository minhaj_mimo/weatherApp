import {Component, Input} from 'angular2/core';
import {WeatherItem} from './weather';

@Component({
    selector: 'weather-item',
    template: `
        <div class="col s12 m6 l4">
          <div class="card blue darken-3">
            <div class="card-content white-text">
              <span class="card-title">{{ weatherItem.city }}</span>
              <h3>{{ weatherItem.temperature }}</h3>
              <p> {{ weatherItem.date | date:'yyyy-MM-dd HH:mm:ss Z' }} </p>
            </div>
          </div>
        </div>
    `,
    inputs:['weatherItem: item']
})

export class ItemComponent{
    // @Input('item') weatherItem: WeatherItem;

    //weatherItem : WeatherItem;

    // constructor(){
    //     this.weatherItem = new WeatherItem('Bangladesh',32,'10 am Sunday, 11/15/2016');
    // }
}

/***************************************************************
 * Component for weather items. The cards are genereting here. *
 ***************************************************************/