import {Component,OnInit} from 'angular2/core';
import {ItemComponent} from './item.component';
import {WeatherItem} from './weather';
import {WEATHER_ITEMS} from './data';
import {WeatherService} from './service';

@Component({
    selector: 'weather-list',
    template: `
        <div class="row">
            <weather-item *ngFor="#weatherItem of weatherItems" [item]="weatherItem"></weather-item>
        </div>
    `,
    directives: [ItemComponent],
    providers: [WeatherService]
})

export class ListComponent implements OnInit{
    weatherItems : WeatherItem[];

    constructor (private _weatherService: WeatherService){}

    ngOnInit():any{
        this.weatherItems = this._weatherService.getWeatherItems();
    };
}


/****************************************
 * Weather Item List is genereting here *
 ****************************************/