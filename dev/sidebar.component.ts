import {Component, Input} from 'angular2/core';
import {Profile} from './profile';
import {ProfileService} from './profile.service';
import {WeatherService} from './weatherApp/service';
import {WeatherItem} from './weatherApp/weather';

@Component({
    selector: 'sidebar',
    template: `
            <div class="row">
                <div class="input-field col s5"></div>

                <div class="col s3">
                    <button (click)="onSaveNew()" class="btn waves-effect waves-light" type="button">Save Profile</button>
                </div>
            </div>

            <div class="row">
                <div class="col s12">
                    <ul class="collection with-header">
                        <li class="collection-header"><h4>Saved Profile</h4></li>
                        <li class="collection-item" *ngFor="#profile of profiles" (click)="onLoadProfile(profile)">
                            <a>
                            <div class="bottomLinks">
                                <h5>{{ profile.profileName }}</h5> 
                                {{ profile.cities.join(', ') }} 
                                <a (click)="onDeleteProfile($event,profile)" class="secondary-content waves-effect waves-light red btn">Delete</a>
                            </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
    `,
    styleUrls:['assets/css/sidebar.css'],
    providers: [ProfileService]
})

export class SidebarComponent{
    profiles: Profile[];

    constructor (private _profileService: ProfileService, private _weatherService:WeatherService){}

    onSaveNew(){
        const cities = this._weatherService.getWeatherItems().map(function(element:WeatherItem){
            return element.city;
        });
        this._profileService.saveNewProfile(cities);
    }

    onLoadProfile(profile:Profile)
    {
        this._weatherService.clearWeatherItems();

        for(let i=0; i<profile.cities.length;i++){
            this._weatherService.searchWeatherData(profile.cities[i])
                .retry()
                .subscribe(
                    data => {
                        const weatherItem = new WeatherItem(
                            data.name,
                            data.main.temp,
                            data.dt
                        );
                        this._weatherService.addWeatherItem(weatherItem);
                    }
                );
        }
    }

    onDeleteProfile(event: Event, profile: Profile)
    {
        event.stopPropagation();
        this._profileService.deleteProfile(profile);
    }

    ngOnInit(){
        this.profiles = this._profileService.getProfiles();
    }


}